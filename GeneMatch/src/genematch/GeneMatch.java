/*
 * Choose license.
 */
package genematch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Andrew
 */
public class GeneMatch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedReader buffer = null;
        {
            PrintWriter outfile = null;
            try {
                if( args.length != 2 )
                {
                    System.out.println("Usage: java -jar \"gene_match\" <original_filename> <assembled_filename>");
                    System.exit(0);
                }
                HashSet<String> assembled_set = new HashSet<String>(2000);
                String filename = args[0];
                String assembled = args[1];
                File file = new File(filename);
                String path = file.getAbsolutePath();
                String dir = path.substring(0, path.lastIndexOf(java.io.File.separatorChar))+java.io.File.separatorChar+"results"+java.io.File.separatorChar;
                File resultdir = new File(dir);
                resultdir.mkdir();
                outfile = new PrintWriter(new FileWriter(dir+"nonmatched.txt"));
                File assembled_file = new File(assembled);
                String assembled_path = assembled_file.getAbsolutePath();
                buffer = new BufferedReader(new FileReader(assembled_path));
                String name = "", line;
                System.out.println("Matching...");
                while( buffer.ready() )
                {
                    line = buffer.readLine();
                    if( line.charAt(0) == '>' )
                        name = line;
                    else
                    {
                       assembled_set.add(name);
                    }
                }
                buffer = new BufferedReader(new FileReader(path));
                while( buffer.ready() )
                {
                    line = buffer.readLine();
                    if( line.charAt(0) == '>' )
                    {
                        if( !assembled_set.contains(line) )
                        {
                            outfile.println(line);
                            line = buffer.readLine();
                            outfile.println(line);
                        }
                    }
                }
                System.out.println("Matching complete. Please see results\\nonmatched.txt");
            }catch (FileNotFoundException ex) {
               Logger.getLogger(GeneMatch.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(GeneMatch.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    buffer.close();
                } catch (IOException ex) {
                    Logger.getLogger(GeneMatch.class.getName()).log(Level.SEVERE, null, ex);
                }
                outfile.close();
            }
        }
    }
}
