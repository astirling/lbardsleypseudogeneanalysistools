/*
 * Choose license.
 */
package pileup2fasta;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Andrew
 */
public class Pileup2Fasta {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedReader data_buffer = null;
        File input_file = null;
        String input;
        String[] tokens;
        int max_cols = 500;
        if( args.length < 1 )
        {
            System.out.println("Error: Please supply a pileup file.");
        }
        input_file = new File(args[0]);
        if( !input_file.exists() || !input_file.isFile() )
        {
            System.out.println("Error: File does not exist or is a directory.");
            input_file = null;
        }
        if( args.length > 1 )
        {
            max_cols = Integer.parseInt(args[1]);
            System.out.println("Max columns set to "+max_cols+"...");
        }
        try {
            String path = input_file.getAbsolutePath();
            String dir = path.substring(0, path.lastIndexOf(java.io.File.separatorChar))+java.io.File.separatorChar+"results"+java.io.File.separatorChar;
            File resultdir = new File(dir);
            resultdir.mkdir();
            PrintWriter outfile;
            outfile = new PrintWriter(new FileWriter(dir+input_file.getName()+".fasta"));
            data_buffer = new BufferedReader(new FileReader(input_file.getAbsoluteFile()));
            ArrayList<String> current = new ArrayList<String>();
            System.out.println("Converting...");
            int namenum = 1;
            for( String offset = ""; data_buffer.ready() && !(input = data_buffer.readLine()).isEmpty(); offset = offset + "-" )
            {
                tokens = input.split("\t");
                String line = tokens[4];
                LinkedList<Atom> lineList = parse(line);
                Iterator<Atom> iterator = lineList.iterator();
                for(int colnum = 0; iterator.hasNext(); colnum++)
                {
                    Atom next = iterator.next();
                    if( next.isStart() )
                    {
                        current.add(colnum, offset);
                    }
                    if( next.getEntry() == '*' )
                    {
                        current.set(colnum, current.get(colnum)+"-");
                    }
                    else if( next.getEntry() == ',' || next.getEntry() == '.' )
                    {
                        current.set(colnum, current.get(colnum)+tokens[2].toUpperCase());
                    }
                    else
                    {
                        current.set(colnum, current.get(colnum)+Character.toUpperCase(next.getEntry()));
                    }
                    if( next.isEnd() )
                    {
                        // Delete as we go to save on memory.
                        outfile.println(">"+String.format("%06d", namenum) );
                        outfile.println(current.get(colnum));
                        namenum++;
                        current.remove(colnum);
                        colnum--;
                    }
                }
            }
            System.out.println("Conversion complete!");
        }
        catch (IOException ex) {
            Logger.getLogger(Pileup2Fasta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static LinkedList<Atom> parse(String line) {
        LinkedList<Atom> result = new LinkedList<Atom>();
        for( int i = 0; i < line.length(); i++ )
        {
            //System.out.println(i+": "+line.charAt(i));
            boolean start = false;
            if( line.charAt(i) == '^' )
            {
                start = true;
                i += 2;
            }
            Atom next = new Atom(line.charAt(i));
            next.setStart(start);
            if( line.length() > i+1 )
            {
                if( line.charAt(i+1) == '+' || line.charAt(i+1) == '-' )
                {
                    //System.out.println("Handling plus...");
                    i++;
                    int num = 0, j = i+1;
                    for( ; j < line.length(); j++ )
                    {
                        if( !Character.isDigit(line.charAt(j)) )
                        {
                            num = Integer.parseInt(line.substring(i+1, j));
                            break;
                        }
                    }
                    //System.out.println("Using: "+line.substring(i, j+num));
                    next.setModification(line.substring(i, j+num));
                    i = j+num-1;
                }
                if( line.length() > i+1 && line.charAt(i+1) == '$' )
                {
                    next.setEnd(true);
                    i++;
                }
            }
            result.add(next);
        }
        return result;
    }
}
