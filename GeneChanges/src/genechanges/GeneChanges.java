/*
 * Choose license.
 */
package genechanges;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Andrew
 */
public class GeneChanges {

    private static float minimum_frequency = 0.005f;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedReader buffer = null;
        ArrayList<File> input_files = new ArrayList<File>(); // Note: Ordered.
        InputStreamReader converter = new InputStreamReader(System.in);
        HashMap<Integer, HashMap<String, HashMap<File, Change>>> changes = new HashMap<Integer, HashMap<String, HashMap<File, Change>>>();
        TreeSet<Integer> changed_lines = new TreeSet<Integer>();
        BufferedReader in = new BufferedReader(converter);
        String reference;
        {
            PrintWriter outfile = null;
            try {
                String input = "";
                System.out.println("Please enter the minimum frequency (default "+minimum_frequency+"):");
                input = in.readLine();
                if( input.trim().length() != 0 )
                    minimum_frequency = Float.parseFloat(input);
                while( input.trim().length() != 0 || input_files.isEmpty() )
                {
                    if( input_files.isEmpty() )
                        System.out.println("Please enter a file: ");
                    else
                        System.out.println("Please enter a file, or empty line for finished: ");
                    input = in.readLine();
                    if( input.trim().length() > 0 )
                    {
                        File new_file = new File(input);
                        if( !new_file.exists() || !new_file.isFile() )
                            System.out.println("Error: File does not exist or is a directory.");
                        else
                        {
                            input_files.add(new_file);
                            System.out.println("File added.");
                        }
                    }
                }
                System.out.println("Please enter a reference sequence, or press enter for none:");
                reference = in.readLine().trim().toUpperCase();
                if( reference.length() > 1 && reference.charAt(0) == '>' )
                {
                    reference = reference.substring(1).trim();
                }
                outfile = null;
                for( File file : input_files )
                {
                    String path = file.getAbsolutePath();
                    String dir = path.substring(0, path.lastIndexOf(java.io.File.separatorChar))+java.io.File.separatorChar+"results"+java.io.File.separatorChar;
                    File resultdir = new File(dir);
                    resultdir.mkdir();
                    if( outfile == null ); 
                        outfile = new PrintWriter(new FileWriter(dir+"changes_results.csv"));
                    buffer = new BufferedReader(new FileReader(path));
                    String name = "", line;
                    System.out.println("Locating changes...");
                    while( buffer.ready() )
                    {
                        line = buffer.readLine();
                        String[] tokens = line.split("\t");
                        //System.out.println("Analysing "+tokens[1]);
                        HashMap<String,Change> changesmap = findChanges(tokens[4]);
                        if( !changesmap.isEmpty() )
                        {
                            String output = "";
                            //output += tokens[1]+" ("+tokens[2]+") ->";
                            HashMap<String, HashMap<File, Change>> position;
                            if( !changes.containsKey(Integer.parseInt(tokens[1])) )
                            {
                                position = new HashMap<String, HashMap<File, Change>>();
                                changes.put(Integer.parseInt(tokens[1]), position);
                            }
                            else
                                position = changes.get(Integer.parseInt(tokens[1]));
                            for( String key : changesmap.keySet() )
                            {
                                HashMap<File, Change> change;
                                if( !position.containsKey(tokens[2].toUpperCase()+"->"+key.toUpperCase()) )
                                {
                                    change = new HashMap<File, Change>();
                                    position.put(tokens[2].toUpperCase()+"->"+key.toUpperCase(), change);
                                }
                                else
                                    change = position.get(tokens[2].toUpperCase()+"->"+key.toUpperCase());
                                Change c = changesmap.get(key);
                                change.put(file, c);
                                changed_lines.add(Integer.parseInt(tokens[1]));
                                //output += " "+key+"("+c.number+","+c.frequency+");";
                            }
                        }
                    }
                    outfile.print(",,");
                    for( File source : input_files )
                    {
                        outfile.printf("%s,%s,", source.getName(),source.getName());
                    }
                    outfile.println();
                    for( Integer position : changed_lines )
                    {
                        for( String change : changes.get(position).keySet() )
                        {
                            outfile.printf("%d,%s,", position, change);
                            for( File source : input_files )
                            {
                                if( changes.get(position).get(change).containsKey(source) )
                                {
                                    Change c = changes.get(position).get(change).get(source);
                                    outfile.printf("%d,%.6f,", c.number, c.frequency);
                                }
                                else
                                    outfile.print(",,");
                            }
                            if( reference.length() > 1 )
                            {
                                for( int i = -3; i <= 3; i++ )
                                {
                                    if( i != 0 && position - 1 + i > 0 && position - 1 + i < reference.length() )
                                        outfile.printf("%c", reference.charAt(position-1+i) );
                                    if( i == 0 )
                                        outfile.print(change);
                                    outfile.print(",");
                                }
                            }
                            outfile.println();
                        }
                    }
                }
                System.out.println("Done. Check the results directory.");
            }catch (FileNotFoundException ex) {
               System.out.println("Error: Could not access results file!");
            } catch (IOException ex) {
                System.out.println("Error in saving results.");
            } finally {
                try {
                    buffer.close();
                } catch (IOException ex) {
                    Logger.getLogger(GeneChanges.class.getName()).log(Level.SEVERE, null, ex);
                }
                outfile.close();
            }
        }
    }

    private static HashMap<String,Change> findChanges(String row) {
        HashMap<String,Integer> changes = new HashMap<String,Integer>();
        int count = 0;
        for(int i = 0; i < row.length(); i++ )
        {
            if( row.charAt(i) == '.' || row.charAt(i) == ',' || row.charAt(i) == 'N'
                    || row.charAt(i) == 'n' || row.charAt(i) == '*' || row.charAt(i) == '$'
                    || row.charAt(i) == '^' )
            {
                if( row.charAt(i) != '$' && row.charAt(i) != 'n' && row.charAt(i) != 'N' )
                    count++;
                continue;
            }
            if( row.charAt(i) == '~' )
                break;
            if( row.charAt(i) == '+' || row.charAt(i) == '-' )
            {
                String sequence = String.valueOf(row.charAt(i));
                i++;
                String num = "";
                boolean notnfound = false;
                while( Character.isDigit(row.charAt(i)) )
                {
                    num += row.charAt(i);
                    i++;
                }
                sequence += num;
                int length = Integer.parseInt(num);
                int j;
                for( j = 0; j < length; j++ )
                {
                    char newChar = Character.toUpperCase(row.charAt(i+j));
                    if( newChar != 'N' )
                        notnfound = true;
                    sequence += newChar;
                }
                if( notnfound )
                {
                    if( !changes.containsKey(sequence) )
                        changes.put(sequence, new Integer(1));
                    else
                        changes.put(sequence, ((Integer)(changes.get(sequence))) + 1);
                    count++;
                }
                i += length-1;
            }
            else
            {
                count++;
                String sequence = String.valueOf(Character.toUpperCase(row.charAt(i)));
                if( !changes.containsKey(sequence) )
                    changes.put(sequence, new Integer(1));
                else
                    changes.put(sequence, ((Integer)(changes.get(sequence))) + 1);
            }
        }
        HashMap<String,Change> result = new HashMap<String,Change>();
        
        for( String key : changes.keySet() )
        {
            float frequency;
            frequency = (float) (changes.get(key).floatValue() * 1.0 / count);
            if( frequency > minimum_frequency )
            {
                Change c = new Change();
                c.number = changes.get(key).intValue();
                c.frequency = frequency;
                result.put(key, c);
            }
        }
        return result;
    }
}
