/*
 * Choose license.
 */
package gene.analysis;

/**
 *
 * @author Andrew
 */
public class AnalysisResult {
    public String gene;
    public String sequence;
    public String name;
    
    public AnalysisResult()
    {
        sequence = "";
    }

    void setSequence(String thissequence, int offset, String metadesc, String metasequence) {
        for( int i = 0; i < offset; i++ )
        {
            sequence += "-";
            if( metasequence.charAt(i) == '-' )
            {
                offset++;
            }
        }
        String thismeta = generateMetaSequence(metadesc, offset);
        int metapos = offset;
        for( int i = 0; i < thissequence.length(); i++ )
        {
//            System.out.println(metapos);
            while( metasequence.charAt(metapos) == '-' && thismeta.charAt(i) != '-')
            {
                sequence += "-";
                metapos++;
            }
            sequence += thissequence.charAt(i);
            metapos++;
        }
    }
    
    String generateMetaSequence(String refseq, int offset)
    {
        int start = 0;
        int number = 0;
        String seq = "";
//        for( int i = 0; i < offset; i++ )
//            seq += ".";
        for( int i = 0; i < refseq.length(); i++ )
        {
            if( !Character.isDigit(refseq.charAt(i)) )
            {
                number = Integer.parseInt(refseq.substring(start, i));
                if( refseq.charAt(i) == 'I' )
                {
                    for( int j = 0; j < number; j++ )
                    {
                        seq = seq+"-";
                    }
                }
                else
                {
                    for( int j = 0; j < number; j++ )
                    {
                        seq = seq+".";
                    }
                }
                start = i+1;
            }
        }
        return seq;
    }
}
