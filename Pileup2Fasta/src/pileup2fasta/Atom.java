package pileup2fasta;

/*
 * Choose license.
 */

/**
 *
 * @author Andrew
 */
public class Atom {
    char entry;
    boolean end;
    boolean start;
    String mod = "";
    
    public Atom(char entry)
    {
        this.entry = entry;
        this.end = false;
        this.start = false;
        switch(Character.toUpperCase(entry) )
        {
            case 'A':
            case 'G':
            case 'C':
            case 'T':
            case 'N':
            case ',':
            case '.':
            case '*':
                break;
            default:
                throw new RuntimeException("Error: invalid character passed to Atom: "+entry);
        }
    }
    
    public char getEntry()
    {
        return entry;
    }
    
    public void setEnd(boolean e)
    {
        end = e;
    }
    
    public boolean isEnd()
    {
        return end;
    }
    
    public void setStart(boolean s)
    {
        start = s;
    }
    
    public boolean isStart()
    {
        return start;
    }
    
    public void setModification(String mod)
    {
        this.mod = mod;
    }
    
    @Override
    public String toString()
    {
        String result = "";
        if( start )
            result = "^~";
        result = result + entry + mod;
        if( end )
            result = result + "$";
        return result;
    }
    
    public boolean isForward()
    {
        if( isNeither() )
            return true;
        switch(entry)
        {
            case 'A':
            case 'N':
            case 'T':
            case 'G':
            case 'C':
            case '.':
               return true;
        }
        return false;
    }
    
    public boolean isNeither()
    {
        if( entry == '*' )
            return true;
        return false;
    }
    
    public boolean isBackwards()
    {
        if( isNeither() )
            return true;
        switch(entry)
        {
            case 'a':
            case 'n':
            case 't':
            case 'g':
            case 'c':
            case ',':
               return true;
        }
        return false;
    }
}
