/*
 * Choose license.
 */
package genesorter2;

/**
 *
 * @author Andrew
 */
public class AnalysisResult {
    public String gene;
    public String sequence;
    public String original_sequence;
    public String name;
    
    private int extra_offset = 0;
    
    public AnalysisResult()
    {
        sequence = "";
    }

    void setSequence(String thissequence, int offset, String metadesc, String metasequence) {
        original_sequence = thissequence;
        for( int i = 0; i < offset; i++ )
        {
            sequence += "-";
            if( metasequence.charAt(i) == '-' )
            {
                offset++;
            }
        }
        int start = 0;
        int number = 0;
        String seq = "";
        int ref = 0;
        for( int i = 0; i < metadesc.length(); i++ )
        {
            if( !Character.isDigit(metadesc.charAt(i)) )
            {
                number = Integer.parseInt(metadesc.substring(start, i));
                if( metadesc.charAt(i) == 'D' )
                {
                    for( int j = 0; j < number; j++ )
                    {
                        seq = seq+"-";
                    }
                }
                else if( metadesc.charAt(i) != 'H' )
                {
                    for( int j = 0; j < number; j++ )
                    {
                        seq = seq+thissequence.charAt(ref);
                        ref++;
                    }
                }
                start = i+1;
            }
        }
        String thismeta = generateMetaSequence(metadesc, offset);
        int metapos = offset;
        for( int i = 0; i < seq.length(); i++ )
        {
            while( metasequence.charAt(metapos) == '-' && thismeta.charAt(i) != '-')
            {
                sequence += "-";
                metapos++;
            }
            sequence += seq.charAt(i);
            metapos++;
        }
    }
    
    String generateMetaSequence(String refseq, int offset)
    {
        int start = 0;
        int number = 0;
        String seq = "";
//        for( int i = 0; i < offset; i++ )
//            seq += ".";
        for( int i = 0; i < refseq.length(); i++ )
        {
            if( !Character.isDigit(refseq.charAt(i)) )
            {
                number = Integer.parseInt(refseq.substring(start, i));
                if( refseq.charAt(i) == 'I' )
                {
                    for( int j = 0; j < number; j++ )
                    {
                        seq = seq+"-";
                    }
                }
                else if( refseq.charAt(i) != 'H' )
                {
                    for( int j = 0; j < number; j++ )
                    {
                        seq = seq+".";
                    }
                }
                start = i+1;
            }
        }
        return seq;
    }
}
