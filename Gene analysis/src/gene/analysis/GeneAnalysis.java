/*
 * Choose license.
 */
package gene.analysis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Andrew
 */
public class GeneAnalysis {

    private static HashMap<String,String> referenceSequences;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        referenceSequences = new HashMap<String,String>();
        if( args.length < 1 )
        {
            System.out.println("Usage: geneanalysis <filename>");
            System.exit(0);
        }
        String filename = args[0];
        File file = new File(filename);
        String path = file.getAbsolutePath();
        String dir = path.substring(0, path.lastIndexOf(java.io.File.separatorChar))+java.io.File.separatorChar+"results"+java.io.File.separatorChar;
        File resultdir = new File(dir);
        // If this directory is not already made, make it.
        resultdir.mkdir();
        HashMap map = new HashMap<String,PrintWriter>();
        PrintWriter outfile;
        {
            try {
                BufferedReader buffer = new BufferedReader(new FileReader(path));
                System.out.println("Building meta-analysis...");
                while( buffer.ready() )
                {
                    updateSequence(buffer.readLine());
                }
                System.out.println("Meta-analysis complete.");
                System.out.println("Aligning entries...");
                buffer = new BufferedReader(new FileReader(path));
                while( buffer.ready() )
                {
                    AnalysisResult result = analyse(buffer.readLine());
                    if( map.containsKey(result.gene) )
                    {
                        outfile = (PrintWriter) map.get(result.gene);
                    }
                    else
                    {
                        outfile = new PrintWriter(new FileWriter(dir+result.gene+".txt"));
                        map.put(result.gene, outfile);
                    }
                    outfile.printf(">%s\n%s\n", result.name, result.sequence);
                }
                System.out.println("Complete. Job finished - see results directory.");
            } catch (FileNotFoundException ex) {
               System.out.println("Error: File not found.");
            } catch (IOException ex) {
                System.out.println("Error: Cannot create output file.");
           } finally {
                for( Object w : (map.values()) )
                {
                    PrintWriter wr = (PrintWriter)w;
                    wr.close();
                }
            }
        }
    }

    private static AnalysisResult analyse(String readLine) {
        String[] tokens = readLine.split("\t");
        AnalysisResult result = new AnalysisResult();
        result.gene = tokens[1];
        int preface = Integer.parseInt(tokens[5]);
        result.setSequence(tokens[19].toUpperCase(), preface, tokens[21], referenceSequences.get(tokens[1]));
        result.name = tokens[9];
        return result;
    }

    private static void updateSequence(String readLine) {
        String[] tokens = readLine.split("\t");
        int refpos = Integer.parseInt(tokens[5]);
        String refseq = tokens[21];
        String metasequence;
        if( referenceSequences.containsKey(tokens[1]) )
        {
            metasequence = referenceSequences.get(tokens[1]);
        }
        else
        {
            metasequence = "";
            for( int i = 0; i < Integer.parseInt(tokens[3]); i++ )
                metasequence += ".";
        }
        int counted = Integer.parseInt(tokens[5]);
        for( int i = 0; i < counted; i++ )
        {
            if( metasequence.charAt(i) == '-')
                counted++;
        }
        int start = 0;
        int number = 0;
        for( int i = 0; i < refseq.length(); i++ )
        {
            int j;
            if( !Character.isDigit(refseq.charAt(i)) )
            {
                number = Integer.parseInt(refseq.substring(start, i));
                if( refseq.charAt(i) == 'I' )
                {
                    for( j = 0; j < number; j++ )
                    {
                        if( metasequence.charAt(counted+j) != '-' )
                            metasequence = metasequence.substring(0, counted+j)+"-"+metasequence.substring(counted+j);
                    }
                    counted += number;
                }
                else
                {
                    for( int k = 0; k < number; k++ )
                    {
                        counted++;
                        for( j = 0; metasequence.charAt(counted+j-1) == '-' && counted+j-1 < metasequence.length(); j++ );
                        counted += j;
                    }
                }
                start = i+1;
            }
            
            
            
            
            
            
            
            
            
            
            
            
/*            if( refseq.charAt(i) == '-' && metasequence.charAt(refpos) != '-' )
            {
//                System.out.println("Adding!");
//                int temp = metasequence.length();
                metasequence = metasequence.substring(0, refpos)+"-"+metasequence.substring(refpos);
//                System.out.println("Diff: "+temp+":"+metasequence.length());
            }
            else if( refseq.charAt(i) != '-' )
            {
//                System.out.println(refpos);
//                System.out.println(tokens[1]+": "+refpos+"/"+metasequence.length());
                while( metasequence.charAt(refpos) == '-' )
                {
//                    System.out.println(refpos+"/"+metasequence.length());
                    refpos++;
                }
            }
            refpos++;*/
        }
        referenceSequences.put(tokens[1], metasequence);
    }
}
