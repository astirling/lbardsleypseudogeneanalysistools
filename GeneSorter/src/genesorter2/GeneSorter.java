/*
 * Choose license.
 */
package genesorter2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Andrew
 */
public class GeneSorter {

    private static String referenceSequence;
    
    // Tabular
    /*private static int NAME_INDEX = 9;
    private static int GENE_INDEX = 1;
    private static int CHANGE_INDEX = 21;
    private static int SEQUENCE_INDEX = 19;
    private static int OFFSET_INDEX = 5;*/
    
    // SAM
    private static int NAME_INDEX = 0;
    private static int GENE_INDEX = 2;
    private static int CHANGE_INDEX = 5;
    private static int SEQUENCE_INDEX = 9;
    private static int OFFSET_INDEX = 3;
    
    private static int reference_length;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        InputStreamReader converter = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(converter);
        PrintWriter ref1_outfile, ref1_outfile_aligned, ref2_outfile, ref2_outfile_aligned, unsure_outfile, unsure_outfile_aligned, unmapped_outfile;
        PrintWriter decision_outfile;
        float same_requirement = 0.6f;
        float opposite_tolerance = 0.1f;
        //HashSet<String> ref1_match = new HashSet<String>();
        //HashSet<String> ref2_match = new HashSet<String>();
        //HashSet<String> unsure_match = new HashSet<String>();
        HashSet<String> matched = new HashSet<String>();
        BufferedReader buffer = null;
        HashSet<File> input_files = new HashSet<File>();
        HashMap<Integer,Character> changes = new HashMap<Integer,Character>();
        HashMap<File, File> originals = new HashMap<File, File>();
        try {
            /*if( args.length != 2 )
            {
                System.out.println("Usage: genesearch <filename> <sequence>");
                System.exit(0);
            }*/
            String input = "";
            System.out.println("Please enter the required presence level (n < 1), or press enter for 0.6:");
            input = in.readLine();
            if( input.trim().length() != 0 )
                same_requirement = Float.parseFloat(input);
            System.out.println("Please enter the opposite tolerance level (n < 1), or press enter for 0.1:");
            input = in.readLine();
            if( input.trim().length() != 0 )
                opposite_tolerance = Float.parseFloat(input);
            while( input.trim().length() != 0 || input_files.isEmpty() )
            {
                if( input_files.isEmpty() )
                    System.out.println("Please enter a SAM file: ");
                else
                    System.out.println("Please enter a SAM file, or empty line for finished: ");
                input = in.readLine();
                if( input.trim().length() > 0 )
                {
                    File new_file = new File(input);
                    if( !new_file.exists() || !new_file.isFile() )
                        System.out.println("Error: File does not exist or is a directory.");
                    else
                    {
                        input_files.add(new_file);
                        System.out.println("File added.");
                        System.out.println("Please enter the original file: ");
                        File change_file = null;
                        while( change_file == null )
                        {
                            input = in.readLine();
                            change_file = new File(input);
                            if( !change_file.exists() )
                            {
                                System.out.println("Could not find original file. Please re-enter:");
                                change_file = null;
                            }
                        }
                        originals.put(new_file, change_file);
                    }
                }
            }
            System.out.println("If a second species is being checked for, please enter the key positions with contrast sequences in ###BBB format, seperated by newlines. An empty line will finish: ");
            input = "hold";
            while( input.trim().length() != 0 )
            {
                //else
                //    System.out.println("Please enter a key position, or empty line for finished: ");
                input = in.readLine();
                if( input.trim().length() > 0 )
                {
                    int pos;
                    for( pos = 0; pos < input.length() && Character.isDigit(input.charAt(pos)); pos++ );
                    int position = Integer.parseInt(input.substring(0, pos));
                    for( int i = 0; pos+i < input.length(); i++ )
                        changes.put(position+i,input.charAt(pos+i));
                }
            }
            for( File input_file : input_files )
            {
                referenceSequence = null;
                buffer = new BufferedReader(new FileReader(input_file));
                System.out.println("Building meta-analysis...");
                String ref1 = buffer.readLine();
                String[] ref1tokens = ref1.split("\t");
                reference_length = ref1tokens[SEQUENCE_INDEX].length();
                String path = input_file.getAbsolutePath();
                String dir = path.substring(0, path.lastIndexOf(java.io.File.separatorChar))+java.io.File.separatorChar+"results"+java.io.File.separatorChar;
                File resultdir = new File(dir);
                // If this directory is not already made, make it.
                resultdir.mkdir();
                ref1_outfile = new PrintWriter(new FileWriter(dir+input_file.getName()+"_ref1.sam"));
                ref1_outfile_aligned = new PrintWriter(new FileWriter(dir+input_file.getName()+"_ref1_aligned.fasta"));
                ref2_outfile = null;
                ref2_outfile_aligned = null;
                unsure_outfile = null;
                unsure_outfile_aligned = null;
                decision_outfile = null;
                if( !changes.isEmpty() )
                {
                    ref2_outfile = new PrintWriter(new FileWriter(dir+input_file.getName()+"_ref2.sam"));
                    ref2_outfile_aligned = new PrintWriter(new FileWriter(dir+input_file.getName()+"_ref2_aligned.fasta"));
                    unsure_outfile = new PrintWriter(new FileWriter(dir+input_file.getName()+"_unsure.sam"));
                    unsure_outfile_aligned = new PrintWriter(new FileWriter(dir+input_file.getName()+"_unsure_aligned.fasta"));
                    decision_outfile = new PrintWriter(new FileWriter(dir+input_file.getName()+"_decisions.txt"));
                }
                unmapped_outfile = new PrintWriter(new FileWriter(dir+input_file.getName()+"_unmapped.txt"));
                buffer.close();
                buffer = new BufferedReader(new FileReader(input_file));
                while( buffer.ready() )
                {
                    try
                    {
                        updateSequence(buffer.readLine());
                    }
                    catch( StringIndexOutOfBoundsException e )
                    {
                        System.out.println("Error! Is reference sequence on line 1? Press enter to retry, or CTRL-C to cancel.");
                        in.readLine();
                        buffer.close();
                        buffer = new BufferedReader(new FileReader(input_file));
                    }
                }
                HashMap<Integer,Character> aligned_changes = new HashMap<Integer, Character>();
                HashMap<Integer, Integer> backwards_reference = new HashMap<Integer, Integer>();
                for( Integer cur : changes.keySet() )
                {
                    int new_position = cur-1; // For getting the index
                    for( int i = 0; i <= new_position; i++ )
                    {
                        if( referenceSequence.charAt(i) == '-' )
                            new_position++;
                    }
                    aligned_changes.put(new_position, changes.get(cur));
                    backwards_reference.put(new_position, cur);
                }
                System.out.println("Meta-analysis complete.");
                System.out.println("Aligning and sorting entries...");
                buffer = new BufferedReader(new FileReader(input_file));
                String ref1_aligned;
//                    LinkedList<Integer> indexes;
                AnalysisResult result = analyse(buffer.readLine());
                ref1_outfile_aligned.printf(">%s\n%s\n", result.name, result.sequence);
                ref1_aligned = result.sequence;
                while( buffer.ready() )
                {
                    String current_line;
                    result = analyse((current_line = buffer.readLine()));
                    matched.add(result.name);
                    if( !changes.isEmpty() )
                    {
                        int first = 0, second = 0, neither = 0;
                        decision_outfile.println("Sorting "+result.name);
                        for( Integer cur : aligned_changes.keySet() )
                        {
                            if( cur < result.sequence.length() && Character.toUpperCase(result.sequence.charAt(cur)) == Character.toUpperCase(ref1_aligned.charAt(cur)) )
                            {
                                decision_outfile.println("At "+backwards_reference.get(cur)+"/"+(cur)+", First with: "+result.sequence.charAt(cur) +" Ref was: "+ref1_aligned.charAt(cur) + " Changed was: "+Character.toUpperCase(aligned_changes.get(cur)));
                                first++;
                            }
                            else if( cur < result.sequence.length() && Character.toUpperCase(result.sequence.charAt(cur)) == Character.toUpperCase(aligned_changes.get(cur)) )
                            {
                                decision_outfile.println("At "+backwards_reference.get(cur)+"/"+(cur)+", Second with: "+result.sequence.charAt(cur) +" Ref was: "+ref1_aligned.charAt(cur) + " Changed was: "+Character.toUpperCase(aligned_changes.get(cur)));
                                second++;
                            }
                            else if( cur < result.sequence.length() && Character.toUpperCase(result.sequence.charAt(cur)) != 'N' &&  result.sequence.charAt(cur) != '-' )
                            {
                                decision_outfile.println("At "+backwards_reference.get(cur)+"/"+(cur)+", Unsure with: "+result.sequence.charAt(cur) +" Ref was: "+ref1_aligned.charAt(cur) + " Changed was: "+Character.toUpperCase(aligned_changes.get(cur)));
                                neither++;
                            }
                        }
                        int total = first + second + neither;
                        decision_outfile.printf("First: %d; Second: %d; Neither: %d; Result: ", first, second, neither);
                        if( first > same_requirement * total && second <= opposite_tolerance * total )
                        {
                            decision_outfile.println("First.");
                            ref1_outfile.println(current_line);
                            ref1_outfile_aligned.printf(">%s\n%s\n", result.name, result.sequence);
                        }
                        else if( second > same_requirement * total && first <= opposite_tolerance * total )
                        {
                            decision_outfile.println("Second.");
                            ref2_outfile.println(current_line);
                            ref2_outfile_aligned.printf(">%s\n%s\n", result.name, result.sequence);
                        }
                        else
                        {
                            decision_outfile.println("Neither.");
                            unsure_outfile.println(current_line);
                            unsure_outfile_aligned.printf(">%s\n%s\n", result.name, result.sequence);
                        }
                    }
                    else
                    {
                        ref1_outfile.println(current_line);
                        ref1_outfile_aligned.printf(">%s\n%s\n", result.name, result.sequence);
                    }
                }
                System.out.println("Alignment Complete.");
                System.out.println("Creating unmatched file...");
                buffer = new BufferedReader(new FileReader(originals.get(input_file)));
                while( buffer.ready() )
                {
                    String input_line = buffer.readLine();
                    if( input_line.charAt(0) == '>' &&
                        !matched.contains( input_line.substring(1) ) )
                    {
                        unmapped_outfile.println(input_line);
                        unmapped_outfile.println(buffer.readLine());
                    }
                }
                ref1_outfile.close();
                if( ref2_outfile != null )
                    ref2_outfile.close();
                ref1_outfile_aligned.close();
                if( ref2_outfile_aligned != null )
                    ref2_outfile_aligned.close();
                if( unsure_outfile != null )
                    unsure_outfile.close();
                if( unsure_outfile_aligned != null )
                    unsure_outfile_aligned.close();
                unmapped_outfile.close();
                if( decision_outfile != null )
                    decision_outfile.close();
            }
            System.out.println("Complete. Job finished - see results directory.");
        } catch (FileNotFoundException ex) {
           System.out.println("Error: File not found.");
        } catch (IOException ex) {
            System.out.println("Error: Cannot create output file.");
       }
    }

    private static AnalysisResult analyse(String readLine) {
        String[] tokens = readLine.split("\t");
        AnalysisResult result = new AnalysisResult();
        //System.out.println("Analysing "+tokens[NAME_INDEX]);
        result.gene = tokens[GENE_INDEX];
        int preface = Integer.parseInt(tokens[OFFSET_INDEX])-1;
        result.setSequence(tokens[SEQUENCE_INDEX].toUpperCase(), preface, tokens[CHANGE_INDEX], referenceSequence);
        result.name = tokens[NAME_INDEX];
        return result;
    }

    private static void updateSequence(String readLine) {
        String[] tokens = readLine.split("\t");
        String refseq = tokens[CHANGE_INDEX];
        String metasequence;
        if( referenceSequence != null )
            metasequence = referenceSequence;
        else
        {
            metasequence = "";
            for( int i = 0; i < reference_length; i++ )
                metasequence += ".";
        }
        int counted = Integer.parseInt(tokens[OFFSET_INDEX])-1;
        for( int i = 0; i < counted; i++ )
        {
            if( metasequence.charAt(i) == '-')
                counted++;
        }
        //System.out.println("Skipped to "+counted);
        int start = 0;
        int number = 0;
        for( int i = 0; i < refseq.length(); i++ )
        {
            //System.out.println(tokens[NAME_INDEX]);
            int j;
            if( !Character.isDigit(refseq.charAt(i)) )
            {
                number = Integer.parseInt(refseq.substring(start, i));
                if( refseq.charAt(i) == 'I' )
                {
                    for( j = 0; j < number; j++ )
                    {
                        if( metasequence.charAt(counted+j) != '-' )
                            metasequence = metasequence.substring(0, counted+j)+"-"+metasequence.substring(counted+j);
                    }
                    counted += number;
                }
                else if( refseq.charAt(i) != 'H' )
                {
                    for( int k = 0; k < number; k++ )
                    {
                        for( j = 0; metasequence.charAt(counted+j) == '-' && counted+j < metasequence.length(); j++ );
                        counted += j+1;
                    }
                }
                //System.out.println("Moved to "+counted);
                start = i+1;
            }
        }
        referenceSequence = metasequence;
    }

    private static LinkedList<Integer> createCompression(String ref1, String ref2) {
        LinkedList<Integer> indexes = new LinkedList<Integer>();
        for( int i = Math.max(ref1.length(), ref2.length())-1; i >= 0 ; i-- )
        {
            if( i >= ref1.length() || i >= ref2.length() || ref1.charAt(i) == '-' || ref2.charAt(i) == '-' )
                indexes.add(i);
        }
        return indexes;
    }
    
    private static String compress(String ref, List<Integer> indexes)
    {
        String result = ref;
        for( Integer index : indexes )
        {
            if( ref.length() > index )
            {
                if( ref.length() > index+1 )
                    result = result.substring(0, index)+result.substring(index+1);
                else
                    result = result.substring(0, index);
            }
        }
        return result;
    }
}

/**
     * From IBM.
     */
    class LevenshteinDistanceMetric {
  /**
   * Calculates the distance between Strings x and y using the
   * <b>Dynamic Programming</b> algorithm.
   */
      public final int distance(String x, String y) {

        int m = x.length();
        int n = y.length();

        int[][] T = new int[m + 1][n + 1];

        T[0][0] = 0;
        for (int j = 0; j < n; j++) {
          T[0][j + 1] = T[0][j] + ins(y, j);
        }
        for (int i = 0; i < m; i++) {
          T[i + 1][0] = T[i][0] + del(x, i);
          for (int j = 0; j < n; j++) {
            T[i + 1][j + 1] =  min(
                T[i][j] + sub(x, i, y, j),
                T[i][j + 1] + del(x, i),
                T[i + 1][j] + ins(y, j)
            );
          }
        }

        return T[m][n];
      }
      private int sub(String x, int xi, String y, int yi) {
        return x.charAt(xi) == y.charAt(yi) ? 0 : 1;
      }
      private int ins(String x, int xi) {
        return 1;
      }
      private int del(String x, int xi) {
        return 1;
      }
      private int min(int a, int b, int c) {
        return Math.min(Math.min(a, b), c);
      }
    }