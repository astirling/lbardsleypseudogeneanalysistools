/*
 * Choose license.
 */
package genediff;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Andrew
 */
public class GeneDiff {

    private static HashMap<Integer,Character> change_set;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedReader buffer = null;
        InputStreamReader converter = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(converter);
        File fasta_file = null;
        File csv_file = null;
        String input = "";
        change_set = new HashMap<Integer,Character>(50);
        try {
            while( fasta_file == null )
            {
                System.out.println("Please enter sequence file (Fasta format):");
                input = in.readLine();
                fasta_file = new File(input);
                if( !fasta_file.exists() || !fasta_file.isFile() )
                {
                    System.out.println("Error: File does not exist or is a directory.");
                    fasta_file = null;
                }
            }
            while( csv_file == null )
            {
                System.out.println("Please enter change set file:");
                input = in.readLine();
                csv_file = new File(input);
                if( !csv_file.exists() || !csv_file.isFile() )
                {
                    System.out.println("Error: File does not exist or is a directory.");
                    csv_file = null;
                }
            }
            buffer = new BufferedReader(new FileReader(csv_file.getAbsoluteFile()));
            while( buffer.ready() )
            {
                String[] input_set = buffer.readLine().split(",");
                input_set[2] = input_set[2].trim();
                try {
                    int pos = Integer.parseInt(input_set[1].trim());
                    if( input_set[2].charAt(3) != '+' && input_set[2].charAt(3) != '-' )
                        change_set.put(pos, input_set[2].toUpperCase().charAt(input_set[2].length()-1));
                }
                catch(NumberFormatException e)
                { // Just continue - want to extract all useful data.
                    
                }
            }
            String path = fasta_file.getAbsolutePath();
            String dir = path.substring(0, path.lastIndexOf(java.io.File.separatorChar))+java.io.File.separatorChar+"results"+java.io.File.separatorChar;
            File resultdir = new File(dir);
            resultdir.mkdir();
            PrintWriter outfile;
            outfile = new PrintWriter(new FileWriter(dir+"diff_results.csv"));
            buffer = new BufferedReader(new FileReader(path));
            String reference_sequence_name = buffer.readLine().substring(1);
            String reference_sequence = buffer.readLine().toUpperCase();
            String first_comp_name = buffer.readLine().substring(1);
            String first_comp_sequence = buffer.readLine().toUpperCase();
            String second_comp_sequence_name = buffer.readLine().substring(1);
            String second_comp_sequence = buffer.readLine().toUpperCase();
            outfile.println(","+reference_sequence_name+","+first_comp_name+","+second_comp_sequence_name+",ChangeSet,");
            int count = 0;
            for( int i = 0; i < reference_sequence.length(); i++ )
            {
                if( reference_sequence.charAt(i) == '-' )
                    continue;
                count++;
                if( first_comp_sequence.length() > i && second_comp_sequence.length() > i &&
                    first_comp_sequence.charAt(i) == second_comp_sequence.charAt(i) &&
                    first_comp_sequence.charAt(i) != '-' &&
                    (first_comp_sequence.charAt(i) != reference_sequence.charAt(i) ||
                     (change_set.containsKey(count) && first_comp_sequence.charAt(i) != change_set.get(count))))
                {
                    outfile.println(count+","+reference_sequence.charAt(i)+","+getValueAt(first_comp_sequence,i)+
                       ","+getValueAt(second_comp_sequence,i)+","+getChangeSetAt(count)+",*");
                }
                else if( 
                        (first_comp_sequence.length() > i && first_comp_sequence.charAt(i) != '-' && first_comp_sequence.charAt(i) != reference_sequence.charAt(i)) ||
                    (second_comp_sequence.length() > i && second_comp_sequence.charAt(i) != '-' && second_comp_sequence.charAt(i) != reference_sequence.charAt(i)) ||
                    (change_set.containsKey(count) && 
                    change_set.get(count) != reference_sequence.charAt(i)))
                {
                    outfile.println(count+","+reference_sequence.charAt(i)+","+getValueAt(first_comp_sequence,i)+
                       ","+getValueAt(second_comp_sequence,i)+","+getChangeSetAt(count)+",");
                }
            }
            outfile.close();
            System.out.println("Finished. Please check the results directory.");
        }
        catch (IOException ex) {
            System.out.println("Error in accessing file locations. Aborting.");
            Logger.getLogger(GeneDiff.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static String getValueAt(String str, int loc)
    {
        if( str.length() >= loc+1 )
            return str.substring(loc, loc+1);
        return "";
    }
    
    private static String getChangeSetAt(int loc)
    {
        if( change_set.containsKey(loc) )
            return change_set.get(loc).toString();
        return "";
    }
}
